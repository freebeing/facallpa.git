import prompt from '@system.prompt';
import JsBridgeX from '../../generated/JsBridgeX.js';
/*import featureAbility from '@ohos.ability.featureAbility';*/

const bridge = new JsBridgeX()

export default {
    data: {
        title: "",
        appInfo: {
            bundleName: "com.huawei.appmarket",
            action: "android.intent.action.VIEW",
            uri: "market://details?id=com.tencent.mm",
            flag: false
        },
        appInfo2: {
            "StartAbilityParameter": {
                "want": {
                    "deviceId": "",
                    "bundleName": "com.huawei.appmarket",
                    "abilityName": "",
                    "uri": "market://details?id=com.tencent.mm",
                    "action": "android.intent.action.VIEW",
                },
            }
        },
        javaInterface: {}
    },
    onInit() {
        this.title = this.$t('strings.world');
    },
    onShow() {
        // onInit生命周期中Java接口对象还未创建完成，请勿在onInit中调用。
        //this.javaInterface = createLocalParticleAbility('com.freesonwill.facallpa.biz.MyLocalParticleAbility');
    },
  /*  async add(a, b) {
        let ret = await this.javaInterface.add(a, b);
        console.log("rst:" + JSON.stringify(ret))
    },

    addAsync(a, b) { //异步方法
        this.javaInterface.addAsync(1, 2, rst => {
            console.log("rst:" + JSON.stringify(rst))
        })
    },*/

    //打开华为应用市场的微信下载页面
    async startAbility(data) {
        bridge.startAbility(data)
    },
  /*  //新的打开方式
    async startAbility2(data) {
        featureAbility.startAbility(data)
    },*/
    add(a, b) {
        bridge.add(a, b).then(ret => {
            prompt.showToast({
                message: `${a}+${b} = ${ret.abilityResult}`
            })
        })
    },
    async add2(a, b) {
        var ret = await bridge.add(a, b);
        prompt.showToast({
            message: `${a}+${b} = ${ret.abilityResult}`
        })
    },
    async add3(a, b) {
        var ret = await this.javaInterface.add3(a, b);
        prompt.showToast({
            message: `${a}+${b} = ${ret.abilityResult}`
        })
    },
    async addCallback(a, b) {
        this.javaInterface.addCallback(a, b, ret => {
            prompt.showToast({
                message: `${a}+${b} = ${JSON.stringify(ret)}`
            })
        });

    },
    async isAppExist(data) {
        let ret = await bridge.isAppExist(data)
        prompt.showToast({
            message: `${data.bundleName} exist: ${ret.abilityResult}`
        })
    }
}
