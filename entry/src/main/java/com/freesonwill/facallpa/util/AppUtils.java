package com.freesonwill.facallpa.util;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.app.Context;
import ohos.utils.net.Uri;

public class AppUtils {

    public static boolean isAppExist(Context context, String packageName) {
        if (context == null) {
            return false;
        }
        try {
            return context.getBundleManager().isApplicationEnabled(packageName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void jumpHuaweiMarket(Ability context, String pkg) {
        String HW_MARKET_PACKAGE_NAME = "com.huawei.appmarket";
        Intent i = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withBundleName(HW_MARKET_PACKAGE_NAME)
                .withAction("android.intent.action.VIEW")
                .withUri(Uri.parse("market://details?id=" + pkg))
                .build();
        i.setOperation(operation);
        context.startAbility(i);
    }

}
