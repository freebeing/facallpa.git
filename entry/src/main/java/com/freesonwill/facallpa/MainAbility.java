package com.freesonwill.facallpa;

import com.freesonwill.facallpa.biz.MyLocalParticleAbility;
import ohos.ace.ability.AceAbility;
import ohos.aafwk.content.Intent;

public class MainAbility extends AceAbility {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        MyLocalParticleAbility.getInstance().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        MyLocalParticleAbility.getInstance().deregister(this);
    }
}
