package com.freesonwill.facallpa.biz;

import ohos.ace.ability.LocalParticleAbility;
import ohos.app.AbilityContext;

public class MyLocalParticleAbility implements LocalParticleAbility {

    private static class Holder {
        private static MyLocalParticleAbility _instance = new MyLocalParticleAbility();
    }

    private MyLocalParticleAbility() {
    }

    public static MyLocalParticleAbility getInstance() {
        return Holder._instance;
    }

    private AbilityContext context;

    public void register(AbilityContext context) {
        this.context = context;
    }

    public void unRegister() {
        this.context = null;
    }

    //interface
    public int add(int a, int b) {
        return a + b;
    }

    public void addAsync(int a, int b, Callback cb) {
        new Thread(() -> {
            int ret = a + b;//假设这里是一个耗时操作
            cb.reply(ret );
        }).start();
    }

    //使用Context
    public boolean isAppExist(String packageName) {
        if (context == null) {
            return false;
        }
        try {
            return context.getBundleManager().isApplicationEnabled(packageName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

}
