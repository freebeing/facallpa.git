package com.freesonwill.facallpa.biz;

import com.freesonwill.facallpa.util.AppUtils;
import com.freesonwill.facallpa.util.LogUtil;
import com.huawei.agconnect.common.TextUtils;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.annotation.f2pautogen.ContextInject;
import ohos.annotation.f2pautogen.InternalAbility;
import ohos.app.AbilityContext;
import ohos.eventhandler.EventRunner;
import ohos.utils.net.Uri;
import ohos.utils.zson.ZSONObject;

@InternalAbility(registerTo = "com.freesonwill.facallpa.MainAbility")
public class JsBridgeX {
    private static final String TAG = "JsBridgeX";
    @ContextInject
    AbilityContext abilityContext;

    public int add(int a, int b) {
        LogUtil.debug(TAG,"isMain:"+(EventRunner.current() == EventRunner.getMainEventRunner()));
        return a + b;
    }

    public boolean isAppExist(String dataString) {
        ZSONObject dataObject = ZSONObject.stringToZSON(dataString);
        String bundleName = dataObject.getString("bundleName");
        String abilityName = dataObject.getString("abilityName");
        boolean ret = AppUtils.isAppExist(abilityContext, bundleName);
        return ret;
    }

    public int startAbility(String dataString) {
        LogUtil.debug(TAG, "dataString:" + dataString);
        ZSONObject dataObject = ZSONObject.stringToZSON(dataString);
        String bundleName = dataObject.getString("bundleName");
        String abilityName = dataObject.getString("abilityName");
        String uri = dataObject.getString("uri");
        String action = dataObject.getString("action");
        Boolean flag = dataObject.getBoolean("flag");
        LogUtil.debug(TAG, "bundleName:" + bundleName + " abilityName:" + abilityName + " flag:" + flag);
        Intent intent = new Intent();
        Intent.OperationBuilder builder = new Intent.OperationBuilder();
        if (!TextUtils.isEmpty(uri) && !TextUtils.isEmpty(action)) {
            builder.withBundleName(bundleName)
                    .withUri(Uri.parse(uri))
                    .withAction(action);
        } else {
            builder.withBundleName(bundleName)
                    .withAbilityName(abilityName);
        }
        if (!flag) {
            builder.withFlags(Intent.FLAG_NOT_OHOS_COMPONENT);
        } else {
            builder.withFlags(Intent.FLAG_ABILITY_NEW_MISSION);
            builder.withFlags(Intent.FLAG_INSTALL_ON_DEMAND);
        }
        Operation oprt = builder.build();
        intent.setOperation(oprt);
        ((Ability) abilityContext).startAbility(intent);
        return 0;
    }


}
